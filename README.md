Please Note: 

This project started as a stopwatch with a database for storing multiple events, split times etc.  I am in the process of transforming it into more of a fitness / workout tracker and record keeper.  Also, please note, the times and splits are currently being saved as string data.  This will be changed in the production fitness app to store all times and splits as longs.  In addition, activity, distances, calories etc will be stored for events / workout sessions.



• Currently the stopwatch records time to 1/100 of a second, records elapsed and split intervals.  An event
is a list of split or lap times and is saved with a name, description and timestamp when created.

• Events can be saved, viewed later, edited and or deleted.

• The event export or Database reset are not currently coded. 

• In addition, event images or avatars are not finished and only a grey placeholder image displays in the list of saved events.




**MyDatabaseAdapter.java** 

*Contains SQLite3 data classes and methods for creating, updating, reading and writing Event data to the database.*



 

**MyArrayAdapter.java**

**MyEventArrayAdapter.java**

**MySplitArrayAdapter.java**

A*re custom adapters are used for the various time and event listings activities.*




**WatchFragment.java**

*Controls the "state" of the stopwatch, handles button listeners, calculates elapsed time, time splits and updates the list of split times as well as updates the last split time.*



**Event.java**

**Split.java**

*Event and Split classes.
An event contains one or more time splits.*