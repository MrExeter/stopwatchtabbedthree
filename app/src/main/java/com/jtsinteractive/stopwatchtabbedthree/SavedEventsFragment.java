package com.jtsinteractive.stopwatchtabbedthree;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class SavedEventsFragment extends Fragment {

    private Bundle mBundle;

    private View rootView;

    private MyDatabaseAdapter myDBHelper;

    private Button mExportSelectedButton;
    private Button mDeleteSelectedButton;
    private Button mSelectDeselectAllButton;
    private Button mResetDBButton;
    private ListView mEventsListView;

    private boolean mSelectAll;
    private boolean mExistingEvent;

    private ArrayList<Event> mEventsArrayList;
    private MyEventArrayAdapter myEventArrayAdapter;

    private static final String EVENT_ID = "EVENT_ID";
    private static final String NEW_EVENT = "NEW_EVENT";
    private static final String EXISTING_EVENT = "EXISTING EVENT";


    @Override
    public void onResume() {
        if (!mExistingEvent) {
            super.onResume();
            RefreshEventsListWithDbAction();
        } else {
            super.onResume();
            mExistingEvent = false;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        mSelectAll = false;
        mExistingEvent = false;

        if(mBundle == null){

            mBundle = new Bundle();
        } else {

            mBundle = savedInstanceState;
        }

        rootView = inflater.inflate(R.layout.fragment_saved_events, container, false);

        //#########################################################################################
        //
        //  Initialize Buttons and controls
        //
        mExportSelectedButton = (Button)rootView.findViewById(R.id.exportSelectedButton);
        mDeleteSelectedButton = (Button)rootView.findViewById(R.id.deleteSelectedButton);
        mSelectDeselectAllButton = (Button)rootView.findViewById(R.id.selectDeselectAllButton);
        mResetDBButton = (Button)rootView.findViewById(R.id.clearResetDbButton);

        mEventsListView = (ListView)rootView.findViewById(R.id.savedEventsListView);

        //#########################################################################################
        //
        //  Initialize DB and retrieve all stored events
        //
        RefreshEventsListWithDbAction();


        //#########################################################################################
        //
        //  Create listener for event list view
        //
        mEventsListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

            //#################################################################################
            //
            //  Retrieve event object and ID
            //
            Event tempEvent = mEventsArrayList.get(position);
            long eventID = tempEvent.getEventID();


            Intent intent = new Intent(getActivity(), SaveActivity.class);

            //######################################################################################
            //
            //  Send event id with intent for DB query
            //
            intent.putExtra(EVENT_ID, eventID);

            //######################################################################################
            //
            //  Identify as new event
            //
            intent.putExtra(NEW_EVENT, false);

            startActivityForResult(intent, 1);

            }
        });

        //#########################################################################################
        //
        //  Initialize Button Listeners
        //
        mExportSelectedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mDeleteSelectedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<Event> tempArrayList = new ArrayList<Event>();

                for(int i = 0; i < mEventsArrayList.size(); i++){

                    if(mEventsArrayList.get(i).isChecked()){
                        tempArrayList.add(mEventsArrayList.get(i));
                    }
                }

                if (tempArrayList.size() > 0) {
                    int returnCode = DeleteSelectedEvents(tempArrayList);

                    if(returnCode > 0){
                        RefreshEventsListWithDbAction();
                        Message.message(getContext(), "Successful delete of "+ tempArrayList.size() + " Records");
                    } else {
                        Message.message(getContext(), "Problem deleting events : Return Code = " + returnCode);
                    }
                }
            }
        });

        mSelectDeselectAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mEventsArrayList.size() > 0) {
                    if(!mSelectAll){

                        mSelectDeselectAllButton.setText(R.string.DeselectTxt);
                        mSelectAll = true;
                    }
                    else {

                        mSelectDeselectAllButton.setText(R.string.SelectTxt);
                        mSelectAll = false;
                    }

                    ToggleCheckedAll(mSelectAll);
                }
            }
        });

        mResetDBButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return rootView;
    }

    private int DeleteSelectedEvents(ArrayList<Event> eventList) {
        //#########################################################################################
        //
        //  Deletes user selected events from the Saved events activity
        //
        int returnCode = -1;

        myDBHelper = new MyDatabaseAdapter(getContext());

        returnCode = myDBHelper.deleteEvents(eventList);

        return returnCode;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //#########################################################################################
        //
        //  Used for return results from save/update or cancel from the save/edit activity
        //
        mExistingEvent = true;

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result");

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Message.message(getContext(), "NOT GOOD!!!");
            }
        }
    }

    private void ToggleCheckedAll(boolean selectAll) {

        for(int i=0; i < mEventsArrayList.size(); i++){
            mEventsArrayList.get(i).setIsChecked(selectAll);
        }
        RefreshEventList();
    }

    private void RefreshEventList() {

        //#########################################################################################
        //
        //  Save index and position of list
        //
        int index = mEventsListView.getFirstVisiblePosition();
        View listView = mEventsListView.getChildAt(0);
        int top = (listView == null) ? 0 : listView.getTop();

        myEventArrayAdapter = new MyEventArrayAdapter(mEventsArrayList);
        mEventsListView.setAdapter(myEventArrayAdapter);

        //#########################################################################################
        //
        //  restore the position of listview
        //
        mEventsListView.setSelectionFromTop(index, top);
    }

    private void RefreshEventsListWithDbAction() {
        myDBHelper = new MyDatabaseAdapter(getActivity());

        try {
            mEventsArrayList = myDBHelper.getAllEventsData();

        } catch (Exception e) {
            Message.message(getContext(), "error getting all data");
            Log.w("StopwatchTabbedThree", e.toString());
        }
        myEventArrayAdapter = new MyEventArrayAdapter(mEventsArrayList);

        mSelectDeselectAllButton.setText(R.string.SelectTxt);
        mSelectAll = false;
        mEventsListView.setAdapter(myEventArrayAdapter);
    }

    public ArrayList<Event> getEventsArrayList() {
        return mEventsArrayList;
    }

    public void setEventsArrayList(ArrayList<Event> mEventsLinkList) {
        this.mEventsArrayList = mEventsLinkList;
    }

}
