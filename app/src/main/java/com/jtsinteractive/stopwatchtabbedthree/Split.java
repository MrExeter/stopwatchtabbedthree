package com.jtsinteractive.stopwatchtabbedthree;

import java.io.Serializable;

public class Split implements Comparable<Split>, Serializable{


    //##############################################################################################
    //
    //  Define properties of a split/lap time
    //
    private long mSplitID;
    private String mSplitName;
    private String mSplitDescription;
    private String mSplitText;
    private long mSplitValue;

    private String mElapsedTotalText;
    private long mElapsedTotalValue;


    //##############################################################################################
    //
    //  Constructors
    //
    public Split( long id,
                  String name,
                  String description,
                  String splitText,
                  long mSplitValue,
                  String elapsedTotalText,
                  long elapsedTotalValue) {

        this.mSplitID = id;
        this.mSplitName = name;
        this.mSplitDescription = description;
        this.mSplitText = splitText;
        this.mSplitValue = mSplitValue;
        this.mElapsedTotalText = elapsedTotalText;
        this.mElapsedTotalValue = elapsedTotalValue;
    }

    public Split(Split split){

        this.mSplitID = split.mSplitID;
        this.mSplitName = split.mSplitName;
        this.mSplitDescription = split.mSplitDescription;
        this.mSplitText = split.mSplitText;
        this.mSplitValue = split.mSplitValue;
        this.mElapsedTotalText = split.mElapsedTotalText;
        this.mElapsedTotalValue = split.mElapsedTotalValue;
    }

    public Split(){


    }

    //##############################################################################################
    //
    //  Getters and setters
    //
    public long getSplitID() {
        return mSplitID;
    }

    public void setSplitID(long mSplitID) {
        this.mSplitID = mSplitID;
    }

    public String getSplitName() {
        return mSplitName;
    }

    public void setSplitName(String mSplitName) {
        this.mSplitName = mSplitName;
    }

    public String getSplitDescription() {
        return mSplitDescription;
    }

    public void setSplitDescription(String mSplitDescription) {
        this.mSplitDescription = mSplitDescription;
    }

    public String getSplitText() {
        return mSplitText;
    }

    public void setSplitText(String mSplitText) {
        this.mSplitText = mSplitText;
    }

    public long getSplitValue() {
        return mSplitValue;
    }

    public void setSplitValue(long mSplitValue) {
        this.mSplitValue = mSplitValue;
    }

    public String getElapsedTotalText() {
        return mElapsedTotalText;
    }

    public void setElapsedTotalText(String newElapsedTotalText) {
        this.mElapsedTotalText = newElapsedTotalText;
    }

    public long getElapsedTotalValue() {
        return mElapsedTotalValue;
    }

    public void setElapsedTotalValue(long newElapsedTotalTime) {
        this.mElapsedTotalValue = newElapsedTotalTime;
    }


    //##############################################################################################
    //
    //  Define custom comparable
    //

    @Override
    public int compareTo(Split another) {

        //##########################################################################################
        //
        //  Ascending order
        //
        if(this.getSplitID() > another.getSplitID()){

            return 1;
        }
        else if (this.getSplitID() < another.getSplitID()) {

            return -1;
        }

        return 0;
    }
}
