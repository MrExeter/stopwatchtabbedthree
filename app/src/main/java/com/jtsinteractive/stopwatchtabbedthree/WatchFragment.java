package com.jtsinteractive.stopwatchtabbedthree;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class WatchFragment extends Fragment {

    private Bundle  mBundle;

    private TextView mChronoTextView;
    private TextView mLastSplitTextView;
    private TextView mLastSplitBlinkLabel;
    private Button mStartButton;
    private Button mStopButton;
    private Button mResetButton;
    private Button mSaveButton;
    private Button mSplitButton;

    private String mTimeStampStr;

    private long    mStartTime;

    private ClockState stopWatchState;
    private Vibrator theVibrator;

    private GridLayout mListViewHeader;

    private ListView splitTimes;

    private int mSplitCount;
    private int     vibrationTime;
    private boolean stopwatchStarted;
    private boolean stopPushed, resetPushed;

    private long init, mNow, time, mDelta, mLast;
    private TextView display;
    private Handler handler;

    private ArrayAdapter<String> theArrayAdapter;

    private MySplitArrayAdapter mySplitArrayAdapter;

    private ArrayList<Split> mArraySplitList;

    private ArrayList<String> listItems;
    private ArrayList<String>       splitList;

    private ClockState  mClockState;

    private Display mDisplay;
    private int mDisplayHeight, mDisplaywidth;
    private Fragment mActivityFragment;



    private Point mSize;


    private Animation blinkAnim;
    private boolean firstSplitTaken;

    private View rootView;

    private final static String SPLIT_DIVIDER = "   =   ";
    private final static String SPLIT_HEADER = "Split  ";



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater,container,savedInstanceState);

        if(mBundle == null){

            mBundle = new Bundle();
        } else {

            mBundle = savedInstanceState;
        }

        //##########################################################################################
        //
        //  Initialize Time Delta and last time to zero
        //
        mDelta = 0L;
        mLast = 0L;

        rootView = inflater.inflate(R.layout.activity_watch, container, false);

        handler = new Handler();

        SetDisplayConstraints();

        mStartButton = (Button) rootView.findViewById(R.id.startButton);
        mStopButton = (Button) rootView.findViewById(R.id.stopButton);
        mResetButton = (Button) rootView.findViewById(R.id.resetButton);
        mSaveButton = (Button) rootView.findViewById(R.id.saveButton);
        mSplitButton = (Button) rootView.findViewById(R.id.splitButton);

        mListViewHeader = (GridLayout)rootView.findViewById(R.id.splitListHeader);

        //buttonController(ClockState.READY);
        setClockState(ClockState.READY);

        //#########################################################################################
        //  Initialize text views
        //
        mLastSplitTextView = (TextView) rootView.findViewById(R.id.lastSplitView);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "DigitaldreamSkew.ttf");
        mLastSplitTextView.setTypeface(font);


        //#########################################################################################
        //  Create the ListView Adapter for saving the split times
        //
        listItems = new ArrayList<String>();
        mArraySplitList = new ArrayList<Split>();

        mSplitCount = 0;

        theArrayAdapter = new MyArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, listItems);
        mySplitArrayAdapter = new MySplitArrayAdapter(mArraySplitList);


        splitTimes = (ListView) rootView.findViewById(R.id.splitTimesListView);


        display = (TextView) rootView.findViewById(R.id.chronoTextView);
        display.setTypeface(font);



        //#########################################################################################
        //  Initialize the state of the stopwatch
        //
        //  Enable the START Button
        //  Disable the STOP and RESET buttons
        //
        stopWatchState = ClockState.READY;
        setClockState(stopWatchState);
        stopwatchStarted = false;


        final Runnable updater = new Runnable() {

            @Override
            public void run() {

                if (stopwatchStarted) {
                    mNow = System.currentTimeMillis();
                    time = mNow - init;

                    String text = "";

                    text = getTimeString(time);

                    // display.setText("t: " + time);
                    display.setText(text);
                    handler.postDelayed(this, 10);
                }
            }
        };


        //#########################################################################################
        //
        //  Start Button Listener
        //
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  Save start moment and date for timestamp
                mTimeStampStr = (String) DateFormat.format("dd-MMM-yyyy - hh:mm:ss A", new Date());

                stopwatchStarted = true;
                setClockState(ClockState.RUNNING);
                init = System.currentTimeMillis();
                handler.post(updater);
            }
        });

        //#########################################################################################
        //
        //  Stop Button Listener
        //
        mStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //theVibrator.vibrate(vibrationTime);
                setClockState(ClockState.STOPPED);
            }
        });

        //#########################################################################################
        //
        //  Split Button Listener
        //
        mSplitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!firstSplitTaken) {
                    firstSplitTaken = true;
                }

                mSplitCount++;
                AddSplit(mSplitCount);
            }
        });


        //#########################################################################################
        //
        //  Save Button Listener
        //

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mSplitCount > 0) {

                    SaveNewEvent();
                } else {

                    mSplitCount++;
                    AddSplit(mSplitCount);
                    SaveNewEvent();
                }
            }
        });


        //#########################################################################################
        //
        //  Reset Button Listener
        //

        mResetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clearAndReset();
            }
        });


        return rootView;
    }

    @NonNull
    private String getTimeString(long time) {

        //#########################################################################################
        //
        //  Given a long, return a formatted time string in the form of
        //  00:00'00"00, hours, minutes, seconds, tenths and hundredths of seconds
        //

        String timeText = "";

        int hours = (int) (time / (3600 * 1000));
        int remaining = (int) (time % (3600 * 1000));
        int minutes = (int) (remaining / (60 * 1000));

        remaining = (int) (remaining % (60 * 1000));

        int seconds = (int) (remaining / 1000);
        remaining = (int) (remaining % (1000));

        int milliSeconds = (int) (remaining / 10);

        if (hours < 10) {
            timeText += "0" + Long.toString(hours) + ":";
        } else {
            timeText += Long.toString(hours) + ":";
        }

        if (minutes < 10) {
            timeText += "0" + Long.toString(minutes) + "\'";
        } else {
            timeText += Long.toString(minutes) + "\'";
        }

        if (seconds < 10) {
            timeText += "0" + Long.toString(seconds) + "\"";
        } else {
            timeText += Long.toString(seconds) + "\"";
        }

        if (milliSeconds < 10) {
            timeText += "0" + Long.toString(milliSeconds);
        } else {
            timeText += Long.toString(milliSeconds);
        }
        return timeText;
    }

    private void clearAndReset() {

        //#########################################################################################
        //
        //  Clear and Reset Stopwatch
        //
        setClockState(ClockState.READY);
        //theVibrator.vibrate(vibrationTime);
        theArrayAdapter.clear();

        mLast = 0L;
        mDelta = 0L;

        mArraySplitList.clear();
        mySplitArrayAdapter = new MySplitArrayAdapter(mArraySplitList);
        splitTimes.setAdapter(mySplitArrayAdapter);

        stopwatchStarted = false;
        String text = "00:00\'00\"00";
        display.setText(text);
        mLastSplitTextView.setText(text);
        mSplitCount = 0;
    }


    private void SaveNewEvent() {

        Intent intent = new Intent(getActivity(), SaveActivity.class);
        String splits = "SPLITS";
        String timeStamp = "TIMESTAMP";
        String eventAdapter = "EVENTADAPTER";

        String newEvent = "NEW_EVENT";

        //#########################################################################################
        //
        //  Identify as new event
        //
        intent.putExtra(newEvent, true);

        //#########################################################################################
        //
        //  Pass Extra Timestamp of activity
        //
        intent.putExtra(timeStamp, mTimeStampStr);

        //#########################################################################################
        //
        //  Pass list view items
        //

        intent.putExtra(splits, mArraySplitList);

        startActivityForResult(intent, 1);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        int crap = 0;

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result");

                clearAndReset();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Message.message(getContext(), "NOT GOOD!!!");
            }
        }


    }

    private void AddSplit(int count) {

        String  splitCount;
        String  splitElapsed;
        String  splitValue;

        if(count == 1){
            //  First split
            mLast = mNow;
            mDelta = 0L;

            //splitValue = getTimeString(mDelta);
            splitValue = display.getText().toString();
        }
        else {
            //  Subsequent split
            mDelta = mNow - mLast;
            mLast = mNow;

            splitValue = getTimeString(mDelta);
        }

        //  make header list visible
        mListViewHeader.setVisibility(View.VISIBLE);

        Split aSplit = new Split();

        splitCount = Integer.toString(count);
        splitElapsed = display.getText().toString();

        mLastSplitTextView.setText(splitValue);

        aSplit.setSplitName(Integer.toString(count));
        aSplit.setSplitText(splitValue);
        aSplit.setSplitValue(mDelta);
        aSplit.setElapsedTotalText(splitElapsed);
        aSplit.setElapsedTotalValue(mLast);

        mArraySplitList.add(aSplit);

        ArrayList<Split> tempReverseSplitList = new ArrayList<Split>(mArraySplitList);

        Collections.reverse(tempReverseSplitList);

        //mySplitArrayAdapter = new MySplitArrayAdapter(mArraySplitList);

        mySplitArrayAdapter = new MySplitArrayAdapter(tempReverseSplitList);

        splitTimes.setAdapter(mySplitArrayAdapter);
    }

    private void SetDisplayConstraints() {
        mActivityFragment = this;
        mSize = new Point();
        mActivityFragment.getActivity().getWindowManager().getDefaultDisplay().getSize(mSize);
        mDisplayHeight = mSize.y;
        mDisplaywidth = mSize.x;
    }




    private void setClockState(ClockState newState) {

        //  Set state
        stopWatchState = newState;

        LinearLayout.LayoutParams linearLayoutParams;

        ViewGroup.LayoutParams mStartButtonLayoutParams;
        ViewGroup.LayoutParams  mStopButtonLayoutParams;
        ViewGroup.LayoutParams  mResetButtonLayoutParams;
        ViewGroup.LayoutParams  mSaveButtonLayoutParams;
        ViewGroup.LayoutParams  mSplitButtonLayoutParams;

        switch (newState){
            //  Set the state of the buttons

            case READY:

                mLastSplitBlinkLabel = (TextView) rootView.findViewById(R.id.lastSplitLabel);
//                blinkAnim = new AlphaAnimation(0.0f, 8.0f);
//
//                blinkAnim.setDuration(500);
//                blinkAnim.setStartOffset(10);
//                blinkAnim.setRepeatMode(Animation.REVERSE);
//                blinkAnim.setRepeatCount(Animation.INFINITE);

                // Initially set invisible, only blink when split
                mLastSplitBlinkLabel.setVisibility(View.INVISIBLE);

                mListViewHeader.setVisibility(View.INVISIBLE);

                firstSplitTaken = false;

                mStartButtonLayoutParams = mStartButton.getLayoutParams();

                linearLayoutParams = (LinearLayout.LayoutParams)mStartButton.getLayoutParams();

                mStartButton.setClickable(true);
                mStartButton.setHapticFeedbackEnabled(true);
                mStartButton.setText("START");


                mStartButtonLayoutParams.width = mDisplaywidth;
                mStartButton.setLayoutParams(linearLayoutParams);

                //  Set visibility of buttons, in this case, only the start button is visible
                mStartButton.setVisibility(View.VISIBLE);
                mStopButton.setVisibility(View.GONE);
                mResetButton.setVisibility(View.GONE);
                mSplitButton.setVisibility(View.GONE);
                mSaveButton.setVisibility(View.GONE);

                stopwatchStarted = false;

                break;


            case RUNNING:

                stopwatchStarted = true;



                //  Get button properties
                mSplitButtonLayoutParams = mSplitButton.getLayoutParams();
                mStopButtonLayoutParams  = mStopButton.getLayoutParams();

                linearLayoutParams = (LinearLayout.LayoutParams)mSplitButton.getLayoutParams();


                //  Start button properties
                mSplitButton.setClickable(true);
                mSplitButton.setHapticFeedbackEnabled(true);


                mSplitButtonLayoutParams.width = mDisplaywidth / 2;

                linearLayoutParams.width = mDisplaywidth/2;

                mSplitButton.setLayoutParams(linearLayoutParams);

                //  Stop button properties
                mStopButton.setClickable(true);

                mStopButton.setHapticFeedbackEnabled(true);

                mStopButtonLayoutParams.width = mDisplaywidth / 2;

                mStopButton.setLayoutParams(linearLayoutParams);


                //  Set visibility of buttons, in this case, both split and stop are visible
                mStartButton.setVisibility(View.GONE);
                mStopButton.setVisibility(View.VISIBLE);
                mResetButton.setVisibility(View.GONE);
                mSplitButton.setVisibility(View.VISIBLE);
                mSaveButton.setVisibility(View.GONE);

                break;


            case STOPPED:

                stopwatchStarted = false;

                mLastSplitBlinkLabel.clearAnimation();

                //  Get button properties
                mSaveButtonLayoutParams = mSaveButton.getLayoutParams();
                mResetButtonLayoutParams  = mResetButton.getLayoutParams();
                linearLayoutParams = (LinearLayout.LayoutParams)mSaveButton.getLayoutParams();

                //  Start button properties
                mSaveButton.setClickable(true);
                mSaveButton.setHapticFeedbackEnabled(true);

                mSaveButtonLayoutParams.width = mDisplaywidth / 2;
                linearLayoutParams.width = mDisplaywidth/2;

                mSaveButton.setLayoutParams(linearLayoutParams);


                mListViewHeader.setVisibility(View.VISIBLE);

                //  Stop button properties
                mStopButton.setClickable(true);
                mStopButton.setHapticFeedbackEnabled(true);

                //  Reset Button properties
                mResetButtonLayoutParams.width = mDisplaywidth / 2;
                mResetButton.setLayoutParams(linearLayoutParams);

                //  Set visibility of buttons, in this case, both split and stop are visible
                mStartButton.setVisibility(View.GONE);
                mStopButton.setVisibility(View.GONE);
                mResetButton.setVisibility(View.VISIBLE);
                mSplitButton.setVisibility(View.GONE);
                mSaveButton.setVisibility(View.VISIBLE);

                break;
        }
    }


    private ClockState getClockState() {

        return mClockState;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
