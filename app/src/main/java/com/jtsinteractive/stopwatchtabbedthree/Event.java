package com.jtsinteractive.stopwatchtabbedthree;

import android.media.Image;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by johnsentz on 10/29/15.
 */
public class Event implements Comparator<Event>{

    //##############################################################################################
    //
    //  Properties of an Event
    //
    private long   mEventID;
    private Image  mEventImage;
    private String mName;
    private String mDescription;
    private String mEventDate;
    private ArrayList<Split> mSplitList;

    private boolean mIsChecked;


    public Event(long mEventID,
                 String mName,
                 String mDescription,
                 String mEventDate) {

        this.mEventID = mEventID;
        this.mName = mName;
        this.mDescription = mDescription;
        this.mEventDate = mEventDate;
        this.mIsChecked = false;
    }

    public Event(Event newEvent){

        this.mEventID = newEvent.getEventID();
        this.mName = newEvent.getName();
        this.mDescription = newEvent.getDescription();
        this.mEventDate = newEvent.getEventDate();
        this.mIsChecked = newEvent.mIsChecked;
        this.mSplitList = newEvent.mSplitList;
    }

    public Event(){


    }

    //##############################################################################################
    //
    //  Getters and setters
    //
    public long getEventID() {
        return mEventID;
    }

    public void setEventID(long mEventID) {
        this.mEventID = mEventID;
    }


    public Image getEventImage() {
        return mEventImage;
    }

    public void setEventImage(Image mEventImage) {
        this.mEventImage = mEventImage;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getEventDate() {
        return mEventDate;
    }

    public void setEventDate(String mEventDate) {
        this.mEventDate = mEventDate;
    }

    public boolean isChecked() {
        return mIsChecked;
    }

    public void setIsChecked(boolean checked) {
        this.mIsChecked = checked;
    }

    public ArrayList<Split> getSplitList() {
        return mSplitList;
    }

    public void setSplitList(ArrayList<Split> mSplitList) {
        this.mSplitList = mSplitList;
    }

    @Override
    public int compare(Event e1, Event e2) {

        //##########################################################################################
        //
        //  Ascending order
        //
        if(e1.getEventID() > e2.getEventID()){

            return 1;
        }
        else if (e1.getEventID() < e2.getEventID()) {

            return -1;
        }

        return 0;
    }
}
