package com.jtsinteractive.stopwatchtabbedthree;

public enum ClockState {

    READY,
    RUNNING,
    STOPPED
}