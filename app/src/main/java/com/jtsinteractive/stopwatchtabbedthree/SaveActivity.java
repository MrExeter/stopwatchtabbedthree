package com.jtsinteractive.stopwatchtabbedthree;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.*;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

//##################################################################################################
//


public class SaveActivity extends AppCompatActivity {

    private MyDatabaseAdapter myDBHelper;

    private Intent  saveIntent;
    private ArrayAdapter<String> mAdapter;
    private ArrayList<String> mListItems;

    private ArrayList<Split> mSplitItems;

    private String  mTimeString;

    private Button mSaveDBbutton;
    private Button mCancelButton;

    private EditText mNameEditText;
    private EditText mDescriptionEditText;
    private TextView mEventDateTextView;

    private ListView mSplitsListView;

    private Context mContext;
    private Intent returnIntent;

    private boolean newEvent;
    private ArrayList<Split> mSplitList;
    private String mEventName;
    private String mEventDescription;

    private Event mExistingEvent;

    private MyEventArrayAdapter myEventArrayAdapter;
    private MySplitArrayAdapter mySplitArrayAdapter;

    private final static String SPLIT_DIVIDER = "   =   ";
    private final static String SPLIT_HEADER = "Split  ";

    private static final String EVENT_ID = "EVENT_ID";
    private static final String NEW_EVENT = "NEW_EVENT";
    private static final String EXISTING_EVENT = "EXISTING EVENT";


    private static final String SPLITS = "SPLITS";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);

        //##########################################################################################
        //
        //  Initialize DB Adapter and connection
        //
        myDBHelper = new MyDatabaseAdapter(this);

        //##########################################################################################
        //
        //  Initialize Buttons and controls
        //
        mSaveDBbutton = (Button) findViewById(R.id.dbSaveButton);
        mCancelButton = (Button) findViewById(R.id.cancelButton);
        mSplitsListView = (ListView) findViewById(R.id.splitTimesListViewSave);

        //##########################################################################################
        //
        //  Initialize TextViews and EditText widgets
        //
        mNameEditText = (EditText) findViewById(R.id.eventNameEditText);
        mDescriptionEditText = (EditText) findViewById(R.id.descriptionEditText);
        mEventDateTextView = (TextView) findViewById(R.id.eventDateTextView);


        //##########################################################################################
        //
        //  Retrieve Intent for
        //
        saveIntent = getIntent();

        //##########################################################################################
        //
        //  Initialize Intent for return (StartActivityForResult)
        //
        returnIntent = new Intent();
        mContext = this;
        newEvent = saveIntent.getBooleanExtra(NEW_EVENT, true);

        if(newEvent){
            //######################################################################################
            //
            //  New Event, retrieve the TimeStamp and Splits from the Extras
            //
            //  Retrieve Intent and Splits ArrayList
            //
            mSplitItems = (ArrayList<Split>)saveIntent.getSerializableExtra(SPLITS);



            //######################################################################################
            //
            //  Retrieve timestamp
            //
            mTimeString = saveIntent.getStringExtra("TIMESTAMP");
            mEventDateTextView.setText(mTimeString);

        } else {

            //######################################################################################
            //
            //  Not a new event
            //
            //  Retrieve EventID, Name, Timestamp, Description and Splits from database
            //
            long eventID = saveIntent.getLongExtra(EVENT_ID, 0);


            //######################################################################################
            //
            //  Assign existing event for on save click listener,
            //
            mExistingEvent = RetrieveExistingEvent(eventID);
        }

        //##########################################################################################
        //
        //  Initialize ArrayAdapter and Insert values
        //

        mySplitArrayAdapter = new MySplitArrayAdapter(mSplitItems);

        mSplitsListView.setAdapter(mySplitArrayAdapter);

        //##########################################################################################
        //
        //  Initialize Button Click Listeners
        //
        mSaveDBbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            //##################################################################################
            //
            //  Default condition will be cancel
            //  returnCode = 1; sucessful insert or update
            //  returnCode = 0; canceled
            //  returnCode = -1; error on insert or update or event
            //
            int returnCode = 0;
            String updateInsertMessage;

            if(newEvent){
                //##############################################################################
                //
                //  Create new event
                //
                returnCode = InsertNewEvent();
                updateInsertMessage = (returnCode > 0) ?  "Insert new event okay" : "Insert event FAILED";

                if( returnCode > 0 ) {
                    returnIntent.putExtra("result", 1);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
                else {
                    returnIntent.putExtra("result", 0);
                    setResult(RESULT_CANCELED, returnIntent);
                    finish();
                }

            } else {
                //##############################################################################
                //
                //  Edit existing event
                //
                RefreshEventFields();
                returnCode = UpdateExistingEvent(mExistingEvent);
                updateInsertMessage = (returnCode > 0) ? "Update event okay" : "Update event failed";

                returnIntent.putExtra(EXISTING_EVENT, true);

                if( returnCode > 0 ) {
                    returnIntent.putExtra("result", 1);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
                else {
                    returnIntent.putExtra("result", 0);
                    setResult(RESULT_CANCELED, returnIntent);
                    finish();
                }

            }
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int poop = 0;

                if(newEvent){
                    setResult(RESULT_CANCELED, returnIntent);
                    returnIntent.putExtra(EXISTING_EVENT, true);
                }
                finish();
            }
        });
    }

    private void RefreshEventFields() {
        //##########################################################################################
        //
        //  Refresh event object fields from Name and Description TextEdits on activity
        //
        mExistingEvent.setName( mNameEditText.getText().toString() );
        mExistingEvent.setDescription( mDescriptionEditText.getText().toString() );
    }


    private Event RetrieveExistingEvent(long eventID) {

        Event event = myDBHelper.getEvent(eventID);
        mEventName = event.getName();
        mEventDescription = event.getDescription();
        mTimeString = event.getEventDate();
        //mSplitList = event.getSplitList();
        mSplitItems = event.getSplitList();


        //##########################################################################################
        //
        //  Set name and timestamp on form
        //
        mNameEditText.setText(mEventName);
        mEventDateTextView.setText(mTimeString);
        mDescriptionEditText.setText(mEventDescription);

        //######################################################################################
        //
        //  Retrieve splits from existing events, extract the string values
        //
        ArrayList<String> tempSplits = new ArrayList<String>();

        return event;
    }

    private int UpdateExistingEvent(Event event) {

        int returnCode = myDBHelper.updateEvent(event);

        return returnCode;
    }

    private int InsertNewEvent() {

        //##########################################################################################
        //
        //  Insert new event
        //
        //  Initialize fields for event and splits record insert
        //
        int returnCode;

        String name = mNameEditText.getText().toString();
        String date = mEventDateTextView.getText().toString();
        String desc = mDescriptionEditText.getText().toString();
        //ArrayList<String> splits = mListItems;
        ArrayList<Split> splits = mSplitItems;


        //##########################################################################################
        //
        //  Perform insert, if returnCode = 1, successful insert, if returnCode = -1, we
        //  have an error
        //
        returnCode = myDBHelper.insertEvent(name, date, desc, mSplitItems);
        return returnCode;
    }

    private void resetForm() {
        mNameEditText.setText("");
        mEventDateTextView.setText("");
        mDescriptionEditText.setText("");
        mListItems.clear();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
