package com.jtsinteractive.stopwatchtabbedthree;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


public class MyDatabaseAdapter {

    //##############################################################################################
    //
    //  Class for creating the database, performing inserts, fetching Event/Split and deletion of data
    //

    MyDBHelper helper;


    public MyDatabaseAdapter(Context context) {

        helper = new MyDBHelper(context);
    }

    public Event getEvent(long eventID){

        //#########################################################################################
        //
        //  Initialize readable database connection
        //
        SQLiteDatabase db = helper.getReadableDatabase();

        //#########################################################################################
        //
        //  Get string value of event ID for query
        //
        //  Create Cursor for select query with where clause matching eventID
        //
        String eventStrID = String.valueOf(eventID);
        Event tempEvent = new Event();

        String[] columns = {MyDBHelper.EVENT_UID,
                MyDBHelper.EVENT_NAME,
                MyDBHelper.EVENT_DESCRIPTION,
                MyDBHelper.EVENT_DATE};

        Cursor cursor = db.query(MyDBHelper.EVENTS_TABLE_NAME, columns, helper.EVENT_UID + " = '"
                + eventStrID + "'", null, null, null, null);

        while (cursor.moveToNext()){

            long cid = cursor.getLong(0);
            String eventName = cursor.getString(1);
            String eventDesc = cursor.getString(2);
            String eventDate = cursor.getString(3);

            tempEvent.setEventID(eventID);
            tempEvent.setName(eventName);
            tempEvent.setDescription(eventDesc);
            tempEvent.setEventDate(eventDate);
        }

        //#########################################################################################
        //
        //  Retrieve splits for this event
        //
        ArrayList<Split> splitArrayList = getSplits(eventID);
        tempEvent.setSplitList(splitArrayList);

        Event anEvent = new Event(tempEvent);

        cursor.close();
        db.close();


        return anEvent;
    }

    public ArrayList<Split> getSplits(long eventID){

        //#########################################################################################
        //
        //  Initialize readable database connection
        //

        ArrayList<Split> splitArrayList = new ArrayList<Split>();

        SQLiteDatabase db = helper.getReadableDatabase();
        String eventStrID = String.valueOf(eventID);


        String[] columns = {MyDBHelper.SPLIT_UID,
                MyDBHelper.SPLIT_NAME,
                MyDBHelper.SPLIT_DESCRIPTION,
                MyDBHelper.SPLIT_TEXT,
                MyDBHelper.SPLIT_VALUE,
                MyDBHelper.ELAPSED_TOTAL_TIME,
                MyDBHelper.ELAPSED_TOTAL_VALUE};

        Cursor cursor = db.query(MyDBHelper.SPLITS_TABLE_NAME, columns, helper.FK_EVENT_ID + " = '"
                + eventStrID + "'", null, null, null, null);


        while (cursor.moveToNext()){

            Split tempSplit = new Split();

            long cid = cursor.getLong(0);
            String splitName = cursor.getString(1);
            String splitDesc = cursor.getString(2);
            String splitText = cursor.getString(3);
            long splitValue  = cursor.getLong(4);
            String elapsedTxt = cursor.getString(5);
            long elapsedValue = cursor.getLong(6);

            tempSplit.setSplitID(cid);
            tempSplit.setSplitName(splitName);
            tempSplit.setSplitDescription(splitDesc);
            tempSplit.setSplitText(splitText);
            tempSplit.setSplitValue(splitValue);
            tempSplit.setElapsedTotalText(elapsedTxt);
            tempSplit.setSplitValue(elapsedValue);

            splitArrayList.add(tempSplit);
        }

        cursor.close();
        db.close();

        return splitArrayList;
    }



    public ArrayList<Event> getAllEventsData(){

        //#########################################################################################
        //
        //  Retrieves all event and split data
        //
        //  Initialize database connection
        //
        SQLiteDatabase db = helper.getWritableDatabase();
        ArrayList<Event> eventArrayList = new ArrayList<Event>();

        //#########################################################################################
        //
        //  Select _id_event, event_name, event_description, event_date
        //
        String[] columns = {MyDBHelper.EVENT_UID,
                MyDBHelper.EVENT_NAME,
                MyDBHelper.EVENT_DESCRIPTION,
                MyDBHelper.EVENT_DATE};

        Cursor cursor = db.query(MyDBHelper.EVENTS_TABLE_NAME, columns, null, null, null, null, null);

        while (cursor.moveToNext()){

            long cid = cursor.getLong(0);
            String eventName = cursor.getString(1);
            String eventDesc = cursor.getString(2);
            String eventDate = cursor.getString(3);

            Event anEvent = new Event(cid, eventName, eventDesc, eventDate);

            eventArrayList.add(anEvent);
        }

        cursor.close();
        db.close();

        return eventArrayList;
    }

    public int insertEvent(String name, String date, String desc, ArrayList<Split> splitList){

        int returnCode = -1;

        //#########################################################################################
        //
        //  Initialize database and content values objects
        //
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(helper.EVENT_NAME, name);
        contentValues.put(helper.EVENT_DESCRIPTION, desc);
        contentValues.put(helper.EVENT_DATE, date);

        //#########################################################################################
        //
        //  Insert event into DB and retrieve rowID if insert is successful
        //
        long id_event = db.insert(helper.EVENTS_TABLE_NAME, helper.EVENT_NAME, contentValues);

        if(id_event > 0){

            //  Fabulous code to insert all of the splits
            insertSplits(id_event, splitList);
            returnCode = 1;
        }
        else {
            returnCode = -1;
        }

        db.close();

        return returnCode;
    }

    public int updateEvent(Event event){


        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        long eventID = event.getEventID();
        String eventIDstr = Long.toString(eventID);

        String newEventName = event.getName();
        String newDescrtiption = event.getDescription();
        String[] whereArgs = { eventIDstr };

        contentValues.put(MyDBHelper.EVENT_NAME, newEventName);
        contentValues.put(MyDBHelper.EVENT_DESCRIPTION, newDescrtiption);

        int count = db.update(MyDBHelper.EVENTS_TABLE_NAME, contentValues, MyDBHelper.EVENT_UID + " =? ", whereArgs);


        db.close();

        return count;
    }


    public int deleteEvent(long theID){

        //#########################################################################################
        //
        //  Initialize database object and query string
        //
        int returnCode = -1;
        String stringID = Long.toString(theID);
        String whereClause = stringID + "=" + helper.EVENT_UID;

        SQLiteDatabase db = helper.getWritableDatabase();
        returnCode = db.delete(helper.EVENTS_TABLE_NAME, whereClause, null);
        db.close();

        return returnCode;
    }

    public int deleteEvents(ArrayList<Event> eventArrayList){

        int returnCode = -1;

        for(int i = 0; i < eventArrayList.size(); i++){
            long eventID = eventArrayList.get(i).getEventID();
            returnCode = deleteEvent(eventID);
        }

        return returnCode;
    }

    public int insertSplits(long eventID, ArrayList<Split> splitList){

        int returnCode = -1;

        //#########################################################################################
        //
        //  Initialize database and content values objects
        //
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        try {

            db.beginTransaction();

            Split aSplit = new Split();
            int index = 0;

            while (index < splitList.size()) {

                contentValues.put(helper.SPLIT_NAME, splitList.get(index).getSplitName());
                contentValues.put(helper.SPLIT_DESCRIPTION, splitList.get(index).getSplitDescription());
                contentValues.put(helper.SPLIT_TEXT, splitList.get(index).getSplitText());
                contentValues.put(helper.SPLIT_VALUE, splitList.get(index).getSplitValue());
                contentValues.put(helper.ELAPSED_TOTAL_TIME, splitList.get(index).getElapsedTotalText());
                contentValues.put(helper.ELAPSED_TOTAL_VALUE, splitList.get(index).getElapsedTotalValue());
                contentValues.put(helper.FK_EVENT_ID, eventID);

                long splitID = db.insert(helper.SPLITS_TABLE_NAME, helper.EVENT_DESCRIPTION, contentValues);
                index++;
            }
            db.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            db.endTransaction();
        }

        db.close();

        return 0;
    }


    public int updateName(String oldName, String newName){

        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MyDBHelper.EVENT_NAME, newName);

        String[] whereArgs = { oldName };

        int count = db.update(MyDBHelper.EVENTS_TABLE_NAME, contentValues, MyDBHelper.EVENT_NAME +" =? ", whereArgs);

        db.close();

        return count;
    }

    public int deleteRow(){

        SQLiteDatabase db = helper.getWritableDatabase();
        String[] whereArgs = {""};
        int count = db.delete(MyDBHelper.EVENTS_TABLE_NAME, MyDBHelper.EVENT_NAME + " =? ", whereArgs);

        return count;
    }

    static class MyDBHelper extends SQLiteOpenHelper {

        //#########################################################################################
        //
        //  Inner class for defining, creating, deleting and updating the events database
        //

        //#########################################################################################
        //
        //  Create Table and column names
        //
        private static final String DATABASE_NAME = "stopwatch";
        private static final String EVENTS_TABLE_NAME = "EVENTS";
        private static final String SPLITS_TABLE_NAME = "SPLITS";

        private static final String ENFORCE_FOREIGN_KEYS_QUERY = "PRAGMA foreign_keys=ON;";

        private static final int DATABASE_VERSION = 2;
        private static final String EVENT_UID = "_id_event";
        private static final String EVENT_NAME = "event_name";
        private static final String EVENT_DESCRIPTION = "event_description";
        private static final String EVENT_DATE = "event_date";

        private static final String SPLIT_UID = "_id_split";
        private static final String SPLIT_NAME = "split_name";
        private static final String SPLIT_DESCRIPTION = "split_description";
        private static final String SPLIT_TEXT = "split_text";
        private static final String SPLIT_VALUE = "split_value";
        private static final String ELAPSED_TOTAL_TIME = "elapsed_total_time";
        private static final String ELAPSED_TOTAL_VALUE = "elapsed_total_value";
        private static final String FK_EVENT_ID = "_id_fk_event";

        //#########################################################################################
        //
        //  Create Events Table string
        //
        private static final String CREATE_EVENTS_TABLE = "CREATE TABLE " + EVENTS_TABLE_NAME + " ("
                + EVENT_UID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + EVENT_NAME + " TEXT, "
                + EVENT_DESCRIPTION + " TEXT, "
                + EVENT_DATE + " TEXT);";

        //#########################################################################################
        //
        //  Create Splits Table string
        //
        private static final String CREATE_SPLITS_TABLE = "CREATE TABLE " + SPLITS_TABLE_NAME + " ("
                + SPLIT_UID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + SPLIT_NAME + " TEXT, "
                + SPLIT_DESCRIPTION  + " TEXT, "
                + SPLIT_TEXT + " TEXT, "
                + SPLIT_VALUE  + " INTEGER, "
                + ELAPSED_TOTAL_TIME + " TEXT, "
                + ELAPSED_TOTAL_VALUE + " INTEGER, "
                + FK_EVENT_ID + " INTEGER, FOREIGN KEY("
                + FK_EVENT_ID + ")" + " REFERENCES "
                + EVENTS_TABLE_NAME + "(" + EVENT_UID + ") ON DELETE CASCADE );";

        //#########################################################################################
        //
        //  Table Drop Scripts
        //
        private static final String DROP_EVENTS_TABLE = "DROP TABLE IF EXISTS " + EVENTS_TABLE_NAME;
        private static final String DROP_SPLITS_TABLE = "DROP TABLE IF EXISTS " + SPLITS_TABLE_NAME;

        private Context theContext;

        public MyDBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.theContext = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // "CREATE TABLE " + EVENTS_TABLE_NAME + " (" + EVENT_UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + EVENT_NAME + " VARCHAR(255));

            try {
                db.execSQL(ENFORCE_FOREIGN_KEYS_QUERY);
                db.execSQL(CREATE_EVENTS_TABLE);
                db.execSQL(CREATE_SPLITS_TABLE);
            } catch (SQLException e) {
                Message.message(theContext, "" + e);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            // Drop table if exists than recreate
            try {
                db.execSQL(DROP_EVENTS_TABLE);
                db.execSQL(DROP_SPLITS_TABLE);
                onCreate(db);
            } catch (SQLException e) {
                Message.message(theContext, "" + e);
            }
        }
    }
}
