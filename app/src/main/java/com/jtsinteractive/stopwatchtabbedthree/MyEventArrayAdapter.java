package com.jtsinteractive.stopwatchtabbedthree;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by johnsentz on 10/28/15.
 */
public class MyEventArrayAdapter extends BaseAdapter{

    ArrayList<Event> eventArrayList;

    private ArrayList<Boolean> status = new ArrayList<Boolean>();

    public MyEventArrayAdapter(ArrayList<Event> eventList) {
        super();

        this.eventArrayList = new ArrayList<Event>(eventList);

        for (int i = 0; i < getCount(); i++) {
            status.add(false);
        }
    }

    @Override
    public int getCount() {
        return eventArrayList.size();
    }

    @Override
    public Object getItem(int position) {

        return eventArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {

        return eventArrayList.get(position).getEventID();
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {

        View row = convertView;
        EventViewHolder holder = null;

        if(row == null){

            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.single_event_row, parent, false);
            holder = new EventViewHolder(row);
            row.setTag(holder);
        }

        else {

            holder = (EventViewHolder) row.getTag();
        }

        final Event temp = eventArrayList.get(position);

        holder.mEventNameTextView.setText(temp.getName());
        holder.mEventDateTextView.setText(temp.getEventDate());
        holder.mEventCheckBox.setChecked(temp.isChecked());

        final int tempPosition = position;

        final EventViewHolder finalHolder = holder;
        holder.mEventCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

            if (isChecked) {
                status.set(position, true);
            } else {
                status.set(position, false);
            }

            eventArrayList.get(position).setIsChecked(status.get(position));
            }
        });

        return row;
    }
}

class EventViewHolder {

    RoundedImageView mEventImageView;
    TextView mEventNameTextView;
    TextView mEventDateTextView;
    CheckBox mEventCheckBox;

    public EventViewHolder(View view) {

        mEventImageView = (RoundedImageView)view.findViewById(R.id.eventImageView);
        mEventNameTextView = (TextView)view.findViewById(R.id.eventNameTextView);
        mEventDateTextView = (TextView)view.findViewById(R.id.eventDateTextView);
        mEventCheckBox = (CheckBox)view.findViewById(R.id.eventCheckBox);
    }
}