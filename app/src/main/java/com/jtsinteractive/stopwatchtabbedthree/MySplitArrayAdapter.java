package com.jtsinteractive.stopwatchtabbedthree;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by johnsentz on 11/10/15.
 */
public class MySplitArrayAdapter extends BaseAdapter {

    ArrayList<Split> splitArrayList;

    public MySplitArrayAdapter(ArrayList<Split> splitList) {
        this.splitArrayList = new ArrayList<Split>(splitList);
    }

    @Override
    public int getCount() {
        return splitArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return splitArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return splitArrayList.get(position).getSplitID();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        SplitViewHolder holder = null;

        if(row == null){

            LayoutInflater inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.single_split_row, parent, false);
            holder = new SplitViewHolder(row);
            row.setTag(holder);
        }
        else {

            holder = (SplitViewHolder) row.getTag();
        }

        final Split temp = splitArrayList.get(position);
        holder.mSplitNumberTextView.setText(temp.getSplitName());
        holder.mSplitValueTextView.setText(temp.getSplitText());
        holder.mElapsedTimeTextView.setText(temp.getElapsedTotalText());

        if (position % 2 == 1) {
            row.setBackgroundColor(Color.parseColor("#202020"));
        } else {
            row.setBackgroundColor(Color.parseColor("#000000"));
        }

        return row;
    }
}

class SplitViewHolder {

    TextView    mSplitNumberTextView;
    TextView    mSplitValueTextView;
    TextView    mElapsedTimeTextView;

    public SplitViewHolder(View view) {

        mSplitNumberTextView = (TextView)view.findViewById(R.id.splitCountTextView);
        mSplitValueTextView = (TextView)view.findViewById(R.id.lastSplitTextView);
        mElapsedTimeTextView =(TextView)view.findViewById(R.id.totalElapsedTextView);
    }
}