package com.jtsinteractive.stopwatchtabbedthree;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

/**
 * Created by johnsentz on 9/1/15.
 */
public class MyArrayAdapter extends ArrayAdapter<String> {


    public MyArrayAdapter(Context context, int resource, ArrayList<String> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = super.getView(position, convertView, parent);

        //######################################################################################
        //
        //  Alternate colors of rows from light to dark
        //
        if (position % 2 == 1) {
            view.setBackgroundColor(Color.parseColor("#202020"));
        } else {
            view.setBackgroundColor(Color.parseColor("#000000"));
        }
        return view;
    }
}
