This Stopwatch has a two tab interface

The first is the Stopwatch face featuring:

    A   large numeric time and split/lap time display

    B   Control buttons for the operation of the stopwatch, Start, Stop, Reset and Save.  The buttons
        are programmatically displayed depending on the current state of the stopwatch.


   1.   Pushing the start button starts the stopwatch and changes the start button into two buttons
        a "Stop" and a "Split" button.

   2.   Pushing the "Split" button will take a split time from the last split time.  The split is
        displayed with the total elapsed time when that split was taken.  They are displayed underneath
        the the buttons in a reverse order, e.g., the most recent splits are listed first and older
        splits are displayed subsequently.


   3.  Pushing the stop button, stops the stopwatch and sets the buttons to a "Save" and "Reset" button

   4.  The "Reset" button clears the stopwatch, splits and resets the display back to "Start"

   5.  The "Save" button takes the user to the Save/Edit screen which allows the user to save the split
        times as an "Event".  The user is prompted to enter a name and brief description for the Event.

        A timestamp of when the event was created is automatically created and displayed as is not editable
        by the user.

   6.  The user has the option to Save or cancel the event.  If the user cancels they are brought back
        to the Stopwatch tab with the current event still displayed.  They can either delete or choose to
        save the event again.

        If the user saves the event, the event is saved and the focus is set to the Stopwatch tab.  The
        user may view the saved event on the Saved Events tab.



The second tab displays the Saved Events

    1.  Saved events are listed.

        NOTE:   The events all have a grey round placeholder image next to each one.  The option to tag an
                image to an event is not coded at this time.

    2.  The user may select an event and that event is then displayed on the Save/Edit activity.  The user
        has the option to edit the event and save or cancel out and go back to the saved events list.

    3.  The user may select multiple checkboxes for either deleting those events or for export.

        NOTE:   The event export feature is not coded yet.